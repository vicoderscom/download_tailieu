<!-- WPDM Template: TEMPLATE VICODERS  -->
<style type="text/css">.more_previews_a{display: block;float: left;margin-right: 6px;}</style>
<div class="template-vicoders">
	<div class="detail-base">
		<div class="col-12 col-sm-3 col-md-3 col-xl-3 text-center img-area">
			[icon_doc]
			<p class="btn-violet">đã tải</p>
			<img class="avatar-item" style="background-image:url([thumb_url_290x300]); " src= "[urltheme]/resources/assets/images/home/item-home.png" alt="">
		</div>
		<div class="col-12 col-sm-9 col-md-9 col-xl-9">
			<div class="col-12 title link-color">
				[title]
			</div>
			<div class="col-12 view-down-group">
				<span class="view-count">
					<i class="fa fa-eye" aria-hidden="true"></i>&nbsp&nbsp [view_count]
				</span> &nbsp &nbsp &nbsp &nbsp
				</span class="donwload-count">
					<i class="fa fa-download" aria-hidden="true"></i> &nbsp [download_count]
				</span>
			</div>
			<div class="col-12 price-group">
				<label class="h4-title">Giá: </label> <span class="price text-red h4-title">50.000 <u>đ</u></span>
			</div>
			<div class="col-12 description">
				[description]
			</div>	
			<div class="col-12 social-likeshare">
				<div class="fb-like" data-href="[page_url]" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
			</div>
			<div class="col-12 btn-buy btn-download ">
				<a href="[download_url]">
					<button type="button" class="btn btn-success btn-md">
						<i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp Download
					</button>
				</a>
			</div>
		</div>
	</div>
	<div class="detail-extend">
		<div class="col-12 doc-preview pad-l-0-i pad-r-0-i">
			[doc_preview]
		</div>
	</div>
</div>
