<?php  
    global $wp_rewrite, $wp_query, $current_user, $wpdb;

    $items_per_page = 30;
    $paged = isset($_GET['trang']) ? ($_GET['trang']) : 0;
    $start = isset($_GET['trang'])?($_GET['trang']-1)*$items_per_page:0;

    $res = $wpdb->get_results("select p.post_title,s.* from {$wpdb->prefix}posts p, {$wpdb->prefix}ahm_download_stats s where s.uid = '{$current_user->ID}' and s.pid = p.ID order by `timestamp` desc limit $start, $items_per_page");
    
    $check_exist = [];
?>
<div class="wrapper-downloadhistory">
    <div class="">
        <div class="h3-title">Thông tin tài liệu đã tải xuống</div>
        <table class="table table-garung table-center table-bordered">
            <thead class="text-center thead">
                <tr class="text-center">
                    <th>STT</th>
                    <th><?php _e('Tài liệu','wpdmpro'); ?></th>
                    <th><?php _e('Giá','wpdmpro'); ?></th>
                    <th><?php _e('Ngày tải','wpdmpro'); ?></th>
                    <th><?php _e('Số lần tải','wpdmpro'); ?></th>
                </tr>
            </thead>
            <tbody>
            <?php
                if(!empty($res)) {
                    $i = 0;
                    foreach($res as $stt => $stat){
                        if(!in_array($stat->pid, $check_exist)) {
                            $check_exist[] = $stat->pid;
                            $get_count_download = $wpdb->get_results("select count(*) as count_pid from {$wpdb->prefix}ahm_download_stats s where s.uid = '{$current_user->ID}' and s.pid = '{$stat->pid}'");
                            ?>
                            <tr>
                                <td><?php echo (++$i); ?></td>
                                <td><a href="<?php echo get_permalink($stat->pid); ?>"><?php echo $stat->post_title; ?></a></td>
                                <td><?php echo price_format(get_package_data($stat->pid, 'base_price')); ?> vnđ</td>
                                <td><?php echo $stat->day . '/' . $stat->month .'/'. $stat->year?></td>
                                <td><?php echo $get_count_download[0]->count_pid; ?></td>
                            </tr>
                            <?php
                        }
                    }
                } else {
                    echo '<tr><td colspan="5">Không có dữ liệu</td></tr>';
                }
            ?>
            </tbody>
        </table>
        <div class="">
            <?php
                $total_pages = (($wpdb->get_var("select count(*) from {$wpdb->prefix}ahm_download_stats s where s.uid = '{$current_user->ID}'")))/$items_per_page;
                // do_action('custom_paginate', $paged, $total_pages);
            ?>
        </div>
    </div>
</div>