<?php
global $current_user, $wpdb;

$res = $wpdb->get_row("select p.* from {$wpdb->prefix}user_extend p where p.user_id = '{$current_user->ID}'");
    
    if(!empty($res->job)) {
        switch ($res->job) {
            case 'hocsinh':
                $res->job = 'Học Sinh';
                break;
            case 'giaovien':
                $res->job = 'Giáo Viên';
                break;
            default:
                $res->job = 'Khác';
                break;
        }
    }

?>
<div class="wrapper-profile">
    <div class="row">
        <div class="col-12 col-sm-6 col-md-6 col-xl-6">
            <div class="h3-title text-red">Thông tin tài khoản</div>
            <table class="table table-garung table-noborder">
                <tr>
                    <td>
                        <?php echo '<b>' . __('Email', 'wpdmpro') . '</b>: ' . $current_user->user_email; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo '<b>' . __('Họ tên', 'wpdmpro') . '</b>: ' . (!empty($current_user->display_name) ? $current_user->display_name : $current_user->user_login); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo '<b>' . __('Năm sinh', 'wpdmpro') . '</b>: ' . (!empty($res->dob) ? $res->dob : 'Chưa cập nhật'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo '<b>' . __('Nghề nghiệp', 'wpdmpro') . '</b>: ' . (!empty($res->job) ? $res->job : 'Chưa cập nhật'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo '<b>' . __('Bộ Môn', 'wpdmpro') . '</b>: ' . (!empty($res->subject) ? $res->subject : 'Chưa cập nhật'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo '<b>' . __('Trường', 'wpdmpro') . '</b>: ' . (!empty($res->school) ? $res->school : 'Chưa cập nhật'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo '<b>' . __('Nơi ở', 'wpdmpro') . '</b>: ' . (!empty($res->address) ? $res->address : 'Chưa cập nhật'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo '<b>' . __('Phone', 'wpdmpro') . '</b>: ' . (!empty($res->phone) ? $res->phone : 'Chưa cập nhật'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo '<b>' . __('Thông tin tài khoản', 'wpdmpro') . '</b>: ' . (!empty($res->payment_info) ? $res->payment_info : 'Chưa cập nhật'); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-xl-6">
            <div class="h3-title text-red">Thống kê chung</div>
            <table class="table table-garung table-noborder">
                <tr>
                    <td>
                        <?php echo '<b>' . __('Số tài liệu đã tải', 'wpdmpro') . '</b>: ' . (number_format($wpdb->get_var("select count(*) from {$wpdb->prefix}ahm_download_stats where uid = '{$current_user->ID}'"),0,'.',',')); ?>
                    </td>
                </tr>
                <tr class="hidden">
                    <td>
                        <?php echo '<b>' . __('Số dư tiết kiệm', 'wpdmpro') . '</b>: ' . (!empty($res->save_money) ? price_format($res->save_money) : '0') . ' VNĐ'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo '<b>' . __('Số dư nạp thẻ', 'wpdmpro') . '</b>: ' . (!empty($res->has_money) ? price_format($res->has_money) : '0') . ' VNĐ'; ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>