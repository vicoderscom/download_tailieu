<?php if(!defined('ABSPATH')) die('!');
global $current_user, $wpdb;

$reg_redirect =  $_SERVER['REQUEST_URI'];
if(isset($params['redirect'])) $reg_redirect = esc_url($params['redirect']);
if(isset($_GET['redirect_to'])) $reg_redirect = esc_url($_GET['redirect_to']);

if(get_option('users_can_register')){
?>
<div class="signup-section">
    <div class="container be-member">
        <div class="row">
            <div class="col-8 col-sm-8 col-md-8 col-xl-8 center-block white-block">
                <div  style="width: 80%;max-width: 98%;margin: 50px auto">
                    <?php if(is_user_logged_in()){
                        do_action("wpdm_user_logged_in","<div class='text-center uppercase' style='color: #63B76C !important;font-weight: 600;'>".__("Đăng nhập thành công.","wpdmpro")."<br style='clear:both;display:block;margin-top:15px'/> <a class='btn btn-xs btn-primary' href='".get_permalink(get_option('__wpdm_user_dashboard'))."'>".__("Trang quản lý cá nhân","wpdmpro")."</a>  <a class='btn btn-xs btn-danger' href='".wp_logout_url('login')."'>".__("Đăng xuất","wpdmpro")."</a></div>");

                    } else {
                    ?>
                        <div class="text-center h3-title text-red member-line-first">Đăng ký tài khoản</div>
                        <div class="text-center member-line-second">Vui lòng nhập đầy đủ thông tin vào ô dưới đây</div>
                        <form method="post" action="" id="registerform" name="registerform" class="login-form">
                        <input type="hidden" name="permalink" value="<?php the_permalink(); ?>" />
                            <?php global $wp_query; if(isset($_SESSION['reg_error'])&&$_SESSION['reg_error']!='') {  ?>
                            <div class="error alert alert-danger">
                            <b>Registration Failed!</b><br/>
                            <?php echo $_SESSION['reg_error']; $_SESSION['reg_error']=''; ?>
                            </div>
                            <?php } ?>

                            <div class="form-group">
                                <label class="uppercase">Email của bạn</label>
                                <div class="input-group">
                                    <input class="form-control " required="required" type="email" size="25" placeholder="<?php _e('E-mail','wpdmpro'); ?>" id="user_email" value="<?php echo isset($_SESSION['tmp_reg_info']['user_email'])?$_SESSION['tmp_reg_info']['user_email']:''; ?>" name="wpdm_reg[user_email]">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="uppercase">Tên đăng nhập</label>
                                <div class="input-group">
                                    <input class="form-control" required="required" placeholder="<?php _e('Username','wpdmpro'); ?>" type="text" size="20" class="required" id="user_login" value="<?php echo isset($_SESSION['tmp_reg_info']['user_login'])?$_SESSION['tmp_reg_info']['user_login']:''; ?>" name="wpdm_reg[user_login]">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="uppercase">Mật khẩu của bạn</label>
                                <div class="input-group">
                                    <input class="form-control" placeholder="<?php _e('Password','wpdmpro'); ?>" required="required" type="password" size="20" class="required" id="password" value="" name="wpdm_reg[user_pass]">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="uppercase">Nhập lại mật khẩu</label>
                                <div class="input-group">
                                    <input class="form-control" required="required" placeholder="<?php _e('Confirm Password','wpdmpro'); ?>" type="password" size="20" class="required" equalto="#password" id="confirm_user_pass" value="" name="confirm_user_pass">
                                </div>
                            </div>

                            <?php do_action("wpdm_register_form"); ?>
                            <?php do_action("register_form"); ?>


                            <input type="hidden" value="" name="redirect_to">
                            <p class="text-center"><button type="submit" class="btn btn-lg btn-yellow" id="registerform-submit" name="wp-submit"> <?php _e('Đăng ký','wpdmpro'); ?></button></p>
                        </form>
                        <hr>
                        <?php _e('Bạn đã tài khoản','wpdmpro'); ?> <a href="<?php echo site_url('/login'); ?>"><?php _e('Click tại đây để đăng nhập','wpdmpro'); ?></a>
                    <?php 
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script language="JavaScript">
  jQuery(function(){       
      jQuery('#registerform').validate({

            highlight: function(label) {
                jQuery(label).closest('.form-group').addClass('has-error');
            },
             success: function(label) {
                label.closest('.form-group').addClass('has-success');
                label.remove();

            }
      });
  });
</script>

    <script>
        jQuery(function ($) {
            var llbl = $('#registerform-submit').html();
            $('#registerform').submit(function () {
                $('#registerform-submit').html("<i class='fa fa-spin fa-spinner'></i> <?php echo __('đang đăng ký...','wpdmpro'); ?>");
                $(this).ajaxSubmit({
                    success: function (res) {
                        if (!res.match(/success/)) {
                            $('form .alert-danger').hide();
                            $('#registerform').prepend("<div class='alert alert-danger'>"+res+"</div>");
                            $('#registerform-submit').html(llbl);
                        } else {
                            location.href = "<?php echo $reg_redirect; ?>";
                        }
                    }
                });
                return false;
            });
        });
    </script>

<?php } else echo "<div class='alert alert-warning'>". __("Đăng ký không được bật!", "wpdmpro")."</div>"; ?>