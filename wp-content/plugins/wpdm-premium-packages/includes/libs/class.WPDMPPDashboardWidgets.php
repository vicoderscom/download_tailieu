<?php

/**
 * User: shahnuralam
 * Date: 5/6/17
 * Time: 7:58 PM
 */
class WPDMPPDashboardWidgets
{

    function __construct(){
        add_action('wp_dashboard_setup', array($this, 'addDashboardWidget'));
    }

    function salesOverview() {
        include WPDMPP_BASE_DIR . '/templates/dashboard-widgets/sales-overview.php';
    }

    function latestOrders() {
        include WPDMPP_BASE_DIR . '/templates/dashboard-widgets/latest-orders.php';
    }

    function recentSales() {
        include WPDMPP_BASE_DIR . '/templates/dashboard-widgets/recent-sales.php';
    }

    function topSales() {
        include WPDMPP_BASE_DIR . '/templates/dashboard-widgets/top-sales.php';
    }


    function addDashboardWidget() {
        wp_add_dashboard_widget('wpdmpp_sales_overview', 'Sales Overview', array($this, 'salesOverview'));
        wp_add_dashboard_widget('wpdmpp_lastest_orders', 'Latest Orders', array($this, 'latestOrders'));
        wp_add_dashboard_widget('wpdmpp_lastest_sales', 'Recently Sold Items', array($this, 'recentSales'));
        wp_add_dashboard_widget('wpdmpp_top_sales', 'Top Selling Items ( Last 90 Days )', array($this, 'topSales'));

    }

}

new WPDMPPDashboardWidgets();