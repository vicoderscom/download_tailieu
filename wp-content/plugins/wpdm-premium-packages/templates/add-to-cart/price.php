<div class="wpmp-regular-price">
<?php
if((double)$sales_price > 0) {
    $sales_price_expire = wpdmpp_sales_price_info($product_id);
    $sales_price = number_format($sales_price, 2);
    $base_price = number_format($base_price, 2);
?>
<div class='wpdm-on-sale'><del><?php echo $currency_sign.$price_html; ?></del> <h3 class="wpdm-sales-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer"><span itemprop="priceCurrency" content="<?php echo wpdmpp_currency_code(); ?>"><?php echo $currency_sign; ?></span><span  class="ttip" title="<?php echo $sales_price_expire; ?>" itemprop="price" id="price-<?php echo $product_id; ?>" class="price-<?php echo $product_id; ?>" content="<?php echo $sales_price; ?>"><?php echo $sales_price; ?></span></h3></div>
<?php
} else { ?>
    <h3 itemprop="offers" itemscope itemtype="http://schema.org/Offer"><span class="price-<?php echo $product_id; ?>" itemprop="price" id="price-<?php echo $product_id; ?>" content="<?php echo $base_price; ?>" ><span itemprop="priceCurrency" content="USD"><?php echo $currency_sign; ?></span><?php echo $price_html; ?></span></span></h3>
<?php } ?>
</div>

