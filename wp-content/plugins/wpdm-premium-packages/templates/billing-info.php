<div class="panel panel-default dashboard-panel">
    <div class="panel-heading"><?php _e('Billing Address','wpdm-premium-package'); ?></div>
    <div class="panel-body">

        <div class="row row-fluid">
            <div class="form-group col-md-6 span6">
                <label class="" for="billing_first_name"><?php _e("First Name", "wpdm-premium-package"); ?> <i class="fa fa-star text-danger ttip" title="<?php _e('Required','wpdm-premium-package'); ?>"></i></label>
                <input type="text" value="<?php if (isset($billing['first_name'])) echo $billing['first_name']; ?>" data-placeholder="<?php _e("Phone", "wpdm-premium-package"); ?>First Name" id="billing_first_name" name="checkout[billing][first_name]" class="input-text required form-control">
                <span class="error help-block"></span>
            </div>
            <div class="form-group col-md-6 span6">
                <label class="" for="billing_last_name"><?php _e("Last Name", "wpdm-premium-package"); ?> <i class="fa fa-star text-danger ttip" title="<?php _e('Required','wpdm-premium-package'); ?>"></i></label>
                <input type="text" value="<?php if (isset($billing['last_name'])) echo $billing['last_name']; ?>" data-placeholder="<?php _e("Phone", "wpdm-premium-package"); ?>Last Name" id="billing_last_name" name="checkout[billing][last_name]" class="input-text required  form-control">
                <span class="error help-block"></span>
            </div>
        </div>

        <div class="row row-fluid">
            <div class="form-group col-md-6 span6">
                <label  class="" for="billing_company"><?php _e("Company Name", "wpdm-premium-package"); ?></label>
                <input type="text" value="<?php if (isset($billing['company'])) echo $billing['company']; ?>" data-placeholder="<?php _e("Company (optional)", "wpdm-premium-package"); ?>" id="billing_company" name="checkout[billing][company]" class="input-text  form-control">
            </div>
            <div class="form-group col-md-6 span6">
                <label class="" for="billing_address_1"><?php _e("Address Line 1", "wpdm-premium-package"); ?> <i class="fa fa-star text-danger ttip" title="<?php _e('Required','wpdm-premium-package'); ?>"></i></label>
                <input type="text" value="<?php if (isset($billing['address_1'])) echo $billing['address_1']; ?>" data-placeholder="<?php _e("Address", "wpdm-premium-package"); ?>" id="billing_address_1" name="checkout[billing][address_1]" class="input-text required  form-control">
                <span class="error help-block"></span>
            </div>
        </div>

        <div class="row row-fluid">
            <div class="form-group col-md-6 span6">
                <label  class="" for="billing_address_2"><?php _e("Address Line 2", "wpdm-premium-package"); ?></label>
                <input type="text" value="<?php if (isset($billing['address_2'])) echo $billing['address_2']; ?>" data-placeholder="<?php _e("Address 2 (optional)", "wpdm-premium-package"); ?>" id="billing_address_2" name="checkout[billing][address_2]" class="input-text  form-control">
            </div>
            <div class="form-group col-md-6 span6">
                <label class="" for="billing_city"><?php _e("Town/City", "wpdm-premium-package"); ?> <i class="fa fa-star text-danger ttip" title="<?php _e('Required','wpdm-premium-package'); ?>"></i></label>
                <input type="text" value="<?php if (isset($billing['city'])) echo $billing['city']; ?>" data-placeholder="<?php _e("Town/City", "wpdm-premium-package"); ?>" id="billing_city" name="checkout[billing][city]" class="input-text required  form-control">
                <span class="error help-block"></span>
            </div>
        </div>

        <div class="row row-fluid">
            <div class="form-group col-md-6 span6">
                <label class="" for="billing_postcode"><?php _e("Postcode/Zip", "wpdm-premium-package"); ?> <i class="fa fa-star text-danger ttip" title="<?php _e('Required','wpdm-premium-package'); ?>"></i></label>
                <input type="text" value="<?php if (isset($billing['postcode'])) echo $billing['postcode']; ?>" data-placeholder="<?php _e("Postcode/Zip", "wpdm-premium-package"); ?>" id="billing_postcode" name="checkout[billing][postcode]" class="input-text required  form-control">
                <span class="error help-block"></span>
            </div>
            <div class="form-group col-md-6 span6">
                <label class="" for="billing_country"><?php _e("Country", "wpdm-premium-package"); ?> <i class="fa fa-star text-danger ttip" title="<?php _e('Required','wpdm-premium-package'); ?>"></i></label>
                <?php
                global $wpdb;
                $countries = $wpdb->get_results("select * from {$wpdb->prefix}ahm_country order by country_name");
                ?>
                <select class="required form-control" id="billing_country" name="checkout[billing][country]">
                    <option value=""><?php _e('--Select a country--','wpdm-premium-package'); ?></option>
                    <?php

                    foreach ($countries as $country) {
                        if(isset($billing['country']) && $billing['country']==$country->country_code) {$selected=' selected="selected"';}
                        else {$selected="";}
                        if (isset($settings['allow_country'])) {
                            foreach ($settings['allow_country'] as $ac) {
                                if ($ac == $country->country_code) {

                                    echo '<option value="' . $country->country_code . '"'.$selected.'>' . $country->country_name . '</option>';
                                    break;
                                }
                            }
                        } else {
                            echo '<option value="' . $country->country_code . '" '.$selected.'>' . $country->country_name . '</option>';
                        }
                    }
                    ?>
                </select>
                <span class="error help-block"></span>
            </div>
        </div>

        <div class="row row-fluid">
            <div class="form-group col-md-6 span6">
                <label class="" for="billing_state"><?php _e("State/County", "wpdm-premium-package"); ?> <i class="fa fa-star text-danger ttip" title="<?php _e('Required','wpdm-premium-package'); ?>"></i></label>
                <input type="text" id="billing_state" name="checkout[billing][state]" data-placeholder="<?php _e("State/County", "wpdm-premium-package"); ?>" value="<?php if (isset($billing['state'])) echo $billing['state']; ?>" class="input-text required  form-control">
                <span class="error help-block"></span>
            </div>
            <div class="form-group col-md-6 span6">
                <label class="" for="billing_email"><?php _e("Email Address", "wpdm-premium-package"); ?> <i class="fa fa-star text-danger ttip" title="<?php _e('Required','wpdm-premium-package'); ?>"></i></label>
                <input type="text" value="<?php if (isset($billing['email'])) echo $billing['email']; ?>" data-placeholder="<?php _e("Email Address", "wpdm-premium-package"); ?>" id="billing_email" name="checkout[billing][email]" class="input-text required email  form-control">
                <span class="error help-block"></span>
            </div>
        </div>

        <div class="row row-fluid">
            <div class="form-group col-md-6 span6">
                <label class="" for="billing_phone"><?php _e("Phone", "wpdm-premium-package"); ?> <i class="fa fa-star text-danger ttip" title="<?php _e('Required','wpdm-premium-package'); ?>"></i></label>
                <input type="text" value="<?php if (isset($billing['phone'])) echo $billing['phone']; ?>" data-placeholder="<?php _e("Phone", "wpdm-premium-package"); ?>" id="billing_phone" name="checkout[billing][phone]" class="input-text required  form-control">
                <span class="error help-block"></span>
            </div>
            <div class="form-group col-md-6 span6">
                <label class="" for="billing_tin"><?php _e("Tax ID #", "wpdm-premium-package"); ?></label>
                <input type="text" value="<?php if (isset($billing['taxid'])) echo $billing['taxid']; ?>" data-placeholder="<?php _e("Tax ID", "wpdm-premium-package"); ?>" id="billing_tin" name="checkout[billing][taxid]" class="input-text required  form-control">
                <span class="error help-block"></span>
            </div>
        </div>

    </div>
</div>
