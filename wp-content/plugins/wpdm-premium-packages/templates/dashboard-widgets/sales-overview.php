<?php
/**
 * User: shahnuralam
 * Date: 5/6/17
 * Time: 8:14 PM
 */
if(!defined('ABSPATH')) die('!');
$daily_sales = wpdmpp_daily_sales('','', date("Y-m-d", strtotime("-6 Days")), date("Y-m-d", strtotime("Tomorrow")));

$date = new DateTime();
$date->modify('this week -6 days');
$fdolw =  $date->format('Y-m-d');

$date = new DateTime();
$date->modify('this week');
$ldolw =  $date->format('Y-m-d');

$date = new DateTime();
$date->modify('first day of last month');
$fdolm = $date->format('Y-m-d');

$date = new DateTime();
$date->modify('first day of this month');
$ldolm = $date->format('Y-m-d');

$dn = 0;

$last_year = date("Y")-1;

?>
<div class="w3eden">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Date', '$', '#'],
                <?php foreach ($daily_sales['sales'] as $date => $sale){ ?>
                ['<?php echo date("D", strtotime($date)); ?>',  <?php echo $sale;?>,      <?php echo $daily_sales['quantities'][$date];?>] <?php if($dn++ < 6) echo ','; else break; ?>
                <?php } ?>
            ]);

            var options = {
                title: 'Last 7 Days Sales',
                hAxis: {title: 'Day',  titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0}
            };

            var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }
    </script>

    <div id="chart_div" style="width: 100%; height: 200px;" class="panel panel-default"></div>



    <div class="row text-center">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body"><h2><?php echo wpdmpp_currency_sign().number_format($daily_sales['sales'][date("Y-m-d")],2); ?></h2></div>
                <div class="panel-footer">Today</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body"><h2><?php echo wpdmpp_currency_sign().number_format($daily_sales['sales'][date("Y-m-d", strtotime("Yesterday"))],2); ?></h2></div>
                <div class="panel-footer">Yesterday</div>
            </div>
        </div>
    </div>
    <div class="list-group" style="margin: 0">

        <div class="list-group-item">
            <span class="badge pull-right"><?php echo wpdmpp_currency_sign().wpdmpp_total_sales('', '', $ldolw, date("Y-m-d", strtotime("Tomorrow")))?></span>
            This Week
        </div>
        <div class="list-group-item">
            <span class="badge pull-right"><?php echo wpdmpp_currency_sign().wpdmpp_total_sales('', '', $fdolw, $ldolw)?></span>
            Last Week
        </div>
        <div class="list-group-item">
            <span class="badge pull-right"><?php echo wpdmpp_currency_sign().wpdmpp_total_sales('', '', date("Y-m-01"), date("Y-m-d", strtotime("Tomorrow")))?></span>
            This Month
        </div>
        <div class="list-group-item">
            <span class="badge pull-right"><?php echo wpdmpp_currency_sign().wpdmpp_total_sales('', '', $fdolm, $ldolm)?></span>
            Last Month
        </div>
        <div class="list-group-item">
            <span class="badge pull-right"><?php echo wpdmpp_currency_sign().number_format(wpdmpp_total_sales('', '', date("Y-01-01"), date("Y-m-d", strtotime("Tomorrow"))),2,'.',',');?></span>
            This Year
        </div>
        <div class="list-group-item">
            <span class="badge pull-right"><?php echo wpdmpp_currency_sign().number_format(wpdmpp_total_sales('', '', "$last_year-01-01", date("Y-01-01")),2,'.',',');?></span>
            Last Year
        </div>
        <div class="list-group-item">
            <span class="badge pull-right"><?php echo wpdmpp_currency_sign().number_format(wpdmpp_total_sales('', '', "1990-01-01", date("Y-m-d", strtotime('Tomorrow'))),2,'.',',');?></span>
            Total
        </div>
    </div>

</div>


