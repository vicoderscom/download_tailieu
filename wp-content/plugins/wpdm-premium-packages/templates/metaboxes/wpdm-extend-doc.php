<?php 
    $doc_grade = get_post_meta($post->ID,'__vicoders_docs_grade',true);
    $source_upload = get_post_meta($post->ID,'__vicoders_source_upload',true);
    $typedoc_upload = get_post_meta($post->ID,'__vicoders_typedoc_upload',true);
    // var_dump($doc_grade); die;
?>
<div class="w3eden extend-doc">
    <div class="row">
        <div class="col-md-12 wpdm-full-front">
            <div class="panel panel-default">
                <div class="panel-heading"><?php _e('Extend infomation','wpdm-premium-package'); ?></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="__wpdm_docs_grade-field"><?php echo __('Lớp','wpdm-premium-package'); ?></label>
                            <select name="docs-grade" id="docs-grade" class="form-control">
                                <option value="10" <?php echo ($doc_grade == 10) ? 'selected="selected"' : ''; ?>>Lớp 10</option>
                                <option value="11" <?php echo ($doc_grade == 11) ? 'selected="selected"' : ''; ?>>Lớp 11</option>
                                <option value="12" <?php echo ($doc_grade == 12) ? 'selected="selected"' : ''; ?>>Lớp 12</option>
                                <option value="0" <?php echo ($doc_grade == 0) ? 'selected="selected"' : ''; ?>>Khác</option>
                            </select>
                            <p style="margin-bottom: 30px;"></p>
                        </div>
                        <div class="col-md-12">
                            <label for="__wpdm_source_upload-field"><?php echo __('Nguồn tài liệu: ','wpdm-premium-package'); ?></label>
                            <input type="radio" name="source-upload" value="1" <?php echo ($source_upload == 1) ? 'checked="checked"' : ''; ?>> Sưu tầm
                                &nbsp &nbsp<input type="radio" name="source-upload" value="2" <?php echo ($source_upload == 2) ? 'checked="checked"' : ''; ?>> Tự làm
                            <p style="margin-bottom: 30px;"></p>
                        </div>
                        <div class="col-md-12">
                            <label for="__wpdm_typedoc_upload-field"><?php echo __('Bộ môn','wpdm-premium-package'); ?></label>
                            <div class="input-group">
                                <input type="radio" name="typedoc-upload" value="1" <?php echo ($typedoc_upload == 1) ? 'checked="checked"' : ''; ?>> Đề thi, bài tập theo dạng <br>
                                <input type="radio" name="typedoc-upload" value="2" <?php echo ($typedoc_upload == 2) ? 'checked="checked"' : ''; ?>> Tài liệu, bài giảng , chuyên đề, sách tham khảo
                            </div>
                            <p style="margin-bottom: 30px;"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>