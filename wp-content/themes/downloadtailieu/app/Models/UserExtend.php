<?php

namespace App\Models;

use NF\Database\Eloquent\Model;

/**
 *
 */
class UserExtend extends Model
{
    /**
     * [$table_name name of table]
     * @var string
     */
    protected $table = PREFIX_TABLE . 'user_extend';

    /**
     * [$primary_id primary key of table]
     * @var string
     */
    protected $primary_key = 'id';

    const STATUS_TRUE  = 1;
    const STATUS_FALSE = 0;

    protected $fillable = ['user_id', 'dob', 'job', 'school', 'payment_info', 'has_money', 'created_at', 'updated_at'];

}
