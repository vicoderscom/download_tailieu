<?php

namespace App\RestApi;

use App\Models\Charge;
use NF\Payment\Baokim;

/**
 * class handle relates post
 */
class BaokimPayment extends \WP_REST_Controller
{
    /**
     * [$base The base to use in the API route]
     * @var string
     */
    protected $rest_base = 'baokim-request';

    /**
     * [$namespace namespace for routes API]
     * @var string
     */
    protected $namespace = 'wp/v2';

    /**
     * [__construct description]
     */
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'register_routes']);
    }

    public function register_routes()
    {
        register_rest_route($this->namespace, "/{$this->rest_base}", [
            'methods'             => \WP_REST_Server::CREATABLE,
            'callback'            => [$this, 'baokim_request'],
            'permission_callback' => [$this, 'get_items_permissions_check'],
        ]);

        register_rest_route($this->namespace, "/baokim-notification", [
            'methods'             => \WP_REST_Server::ALLMETHODS,
            'callback'            => [$this, 'get_items'],
            'permission_callback' => [$this, 'get_items_permissions_check'],
        ]);
    }

    /**
     * [get_items get collection of items ]
     *
     * @param WP_REST_Request $request Full data about the request
     *
     * @return WP_Error|WP_REST_Response
     */
    public function baokim_request($request)
    {
        global $current_user, $wpdb;
        // return $current_user->ID;
        $params = $request->get_params();
        if (empty($params['inp_homenetwork']) || empty($params['txtpin']) && empty($params['txtseri']) ) {
            return;
        }
        $homenetwork = 'VINA';
        switch ($params['inp_homenetwork']) {
            case 'viettel':
                $homenetwork = 'VIETEL';
                break;
            case 'vinaphone':
                $homenetwork = 'VINA';
                break;
            case 'mobiphone':
                $homenetwork = 'MOBI';
                break;
            default:
                $homenetwork = 'VINA';
                break;
        }


        // $params = [
        //     'mang' => $homenetwork,
        //     'pin'  => '1424664709135',
        //     'seri' => '41165434253',
        // ];
        $params_std = [
            'mang' => $homenetwork,
            'pin'  => $params['txtpin'],
            'seri' => $params['txtseri']
        ];

        $baokim = new Baokim($params_std);
        $response = $baokim->sendRequest();

        $handle_response = json_decode($response, true);
        // if($handle_response['status'] == 200) {
        //     $amount = $result['amount'];
        //     $charge = new Charge();
        //     $charge->user_id = $current_user->ID;
        //     $charge->seri_card = $params['txtseri'];
        //     $charge->number_card = $params['txtpin'];
        //     $charge->amount = $result['amount'];
        //     $charge->reason = 'Nạp mới';
        //     $charge->type = 1;
        //     $charge->type_card = $homenetwork;
        //     $charge->status = 1;
        //     $result = $charge->save();
        // }
        $charge = new Charge();
        $charge->user_id = $current_user->ID;
        $charge->seri_card = $params['txtseri'];
        $charge->number_card = $params['txtpin'];
        $charge->amount = 10000;
        $charge->reason = 'Nạp mới';
        $charge->type = 1;
        $charge->type_card = $homenetwork;
        $charge->status = 1;
        $result = $charge->save();

        $response = rest_ensure_response($response);
        return $response;
    }

    /**
     * Check if a given request has access to get items.
     *
     * @param WP_REST_Request $request Full data about the request.
     *
     * @return WP_Error|bool
     */
    public function get_items_permissions_check($request)
    {
        // return current_user_can('edit_posts');
        return true;
    }

    /**
     * Prepare the item for the REST response.
     *
     * @param stdClass        $item    WordPress representation of the item.
     * @param WP_REST_Request $request Request object.
     *
     * @return mixed
     */
    public function prepare_item_for_response($item, $request)
    {

    }
}
