<?php 

namespace App\RestApi;

use App\Models\UserExtend;

/**
* 
*/
class TestMenu extends \WP_REST_Controller
{
	/**
     * [$base The base to use in the API route]
     * @var string
     */
    protected $rest_base = 'get-menu';

    /**
     * [$namespace namespace for routes API]
     * @var string
     */
    protected $namespace = 'wp/v2';
	public function __construct()
	{
		add_action('rest_api_init', [$this, 'register_routes']);
	}

	public function register_routes() {
		register_rest_route($this->namespace, "/{$this->rest_base}", [
            'methods'             => \WP_REST_Server::READABLE,
            'callback'            => [$this, 'update_item'],
            'permission_callback' => [$this, 'update_items_permissions_check'],
        ]);
	}

	public function update_item($request) {
		global $wpdb;
		$params = $request->get_params();
		
		$menu_name = 'main-menu';
		$locations = get_nav_menu_locations();
		$menu_id = $locations[ $menu_name ] ;
		$result = wp_get_nav_menu_object($menu_id);
		$data = wp_get_nav_menu_items('main-menu');
		return new \WP_REST_Response($data, 200);
	}

	public function update_items_permissions_check() {
		return true;
	}
}