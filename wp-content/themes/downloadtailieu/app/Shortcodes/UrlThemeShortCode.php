<?php

namespace App\Shortcodes;

use NF\Abstracts\ShortCode;

class UrlThemeShortCode extends ShortCode
{
    public $name = 'urltheme';

    public function render($attrs)
    {
    	$urltheme = get_stylesheet_directory_uri();
    	return $urltheme;
    }
}
