<?php

namespace App\Taxonomies;

use MSC\Tax;

class GoitietkiemTaxonomy extends Tax
{
	public function __construct()
	{
		$config = [
			'slug' => 'goitietkiem_cate',
			'single' => 'Goitietkiem Category',
			'plural' => 'Goitietkiem Categories'
		];

		$postType = 'goitietkiem';

		$args = [

		];

		parent::__construct($config, $postType, $args);
	}
}