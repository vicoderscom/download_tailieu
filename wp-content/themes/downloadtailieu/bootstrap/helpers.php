<?php

if (!function_exists('view')) {
    /**
     * [view description]
     * @param  [type]  $path [description]
     * @param  array   $data
     * @param  boolean $echo [description]
     * @return [type]        [description]
     */
    function view($path, $data = [], $echo = true)
    {
        if ($echo) {
            echo NF\View\Facades\View::render($path, $data);
        } else {
            return NF\View\Facades\View::render($path, $data);
        }
    }
}

if (!function_exists('asset')) {
    /**
     * [asset description]
     * @param [type] $assets [description]
     */
    function asset($assets)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $assets);
    }
}

if (!function_exists('title')) {
    /**
     *
     * @return string
     */
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('name');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            return $obj->name . ' - ' . get_bloginfo('name');
        }

        if (is_404()) {
            return '404 page not found - ' . get_bloginfo('name');
        }

        return get_the_title() . ' - ' . get_bloginfo('name');
    }
}

if (!function_exists('createExcerptFromContent')) {
    /**
     * this function will create an excerpt from post content
     *
     * @param  string $content
     * @param  int    $limit
     * @param  string $readmore
     * @since  1.0.0
     * @return string $excerpt
     */
    function createExcerptFromContent($content, $limit = 50, $readmore = '...')
    {
        if (!is_string($content)) {
            wp_die(__('first parameter must be a string.', ''));
        }

        if ($content == '') {
            wp_die(__('first parameter is not empty.', ''));
        }

        if (!is_int($limit)) {
            wp_die(__('second parameter must be the number.', ''));
        }

        if ($limit <= 0) {
            wp_die(__('second parameter must greater than 0.'));
        }

        $words = explode(' ', $content);

        if (count($words) <= $limit) {
            $excerpt = $words;
        } else {
            $excerpt = array_chunk($words, $limit)[0];
        }

        return strip_tags(implode(' ', $excerpt)) . $readmore;
    }
}

if (!function_exists('getPostImage')) {
    /**
     * [getPostImage description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? '' : $img[0];
    }
}

if (!function_exists('getLinkForPreview')) {
    function getLinkForPreview($post_id)
    {
        $files = get_package_data($post_id, 'files');
        if (!is_array($files)) {
            return "";
        }

        $ind = -1;
        foreach ($files as $i => $sfile) {
            $ifile = $sfile;
            $sfile = explode(".", $sfile);
            if (in_array(end($sfile), ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx'])) {
                $ind = \WPDM_Crypt::Encrypt($ifile);
                break;
            }
        }

        if ($ind == -1) {
            return "";
        }

        // $url = wpdm_download_url($post_id, 'ind=' . $ind);
        // $permalink   = get_permalink($post_id);
        // $permalink   = urlencode($permalink);
        // $sap         = strpos($permalink, '?') ? '&' : '?';
        // $url_preview = urlencode($permalink . $sap . "wpdmdl={$post_id}&ind={$ind}");

        $permalink = get_permalink($post_id);
        // $permalink   = urlencode($permalink);
        $sap         = strpos($permalink, '?') ? '&' : '?';
        $url_preview = $permalink . $sap . "wpdmdl={$post_id}&ind={$ind}";
        if (strpos($ifile, "://")) {
            $url_preview = $ifile;
        }
        return $url_preview;
    }
}

if (!function_exists('price_format')) {
    function price_format($price, $decima = 0)
    {
        return number_format($price, $decima, ',', '.');
    }
}


if(!function_exists('count_nested_categories')) {
    function count_nested_categories($term_id) {
        $child_term = get_term_children($term_id, 'wpdmcategory');
        if(is_wp_error($child_term)) {
            $child_term = [];
        }
        array_push($child_term, $term_id);
        $arg_post_by_idcat = [
            'post_type'      => 'wpdmpro',
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'tax_query'      => [
                [
                    'taxonomy'         => 'wpdmcategory',
                    'terms'            => $child_term,
                    'field'            => 'term_id',
                    'operator'         => 'IN'
                ],
            ]
        ];
        
        $obj_post = new WP_Query($arg_post_by_idcat);  
        // echo "<pre>";
        // var_dump($obj_post->found_posts);
        // die;
        return $obj_post->found_posts;
    }
}

if(!function_exists('checkChildTerm')) {
    function checkChildTerm($term_id) {
        $child_term = get_term_children($term_id, 'wpdmcategory');
        if(is_wp_error($child_term)) {
            return false;
        } 
        return true;
    }
}

if(!function_exists('getChildTerms')) {
    function getChildTerms($term_id) {
        $child_terms = get_terms([
            'taxonomy'   => 'wpdmcategory',
            'hide_empty' => true,
            'parent'     => $term_id,
        ]);
        return $child_terms;
    }
}
