<?php 

use App\Models\UserExtend;
/**
* Setting extend for theme
*/
class Setting
{
	public function __construct()
	{
        add_action('widgets_init', [$this, 'widget_register']);
        add_action('action_comment', [$this, 'action_comment'], 10, 1);  
        add_filter('pre_get_posts',[$this, 'SearchFilter']);
        add_action( 'after_setup_theme', [$this, 'wpdocs_after_setup_theme'] );

        add_action( 'show_user_profile', [$this, 'addUserExtendMoney'] );
        add_action( 'edit_user_profile', [$this, 'addUserExtendMoney'] );
        add_action( 'personal_options_update', [$this, 'saveUserExtendMoney'] );
        add_action( 'edit_user_profile_update', [$this, 'saveUserExtendMoney'] );
	}

	public function action_comment($id) {
        // if (is_single ()):
        $url = site_url();
        $comments_args = array(
            'fields' => [
                'author' => '<p class="comment-form-author"><input id="author" placeholder="Họ Tên" class="form-control" name="author" required="required" type="text" style="" value="" size="30" /></p>',
                'email' => '<p class="comment-form-email"><input id="email" class="form-control" name="email" required="required" placeholder="Email*" type="text" style="" value="" size="30" /></p>'
            ],
            'label_submit' => __( 'Gửi', 'wdownloadtailieu' ),
            'title_reply' => __( 'BÌNH LUẬN TÀI LIỆU', 'wdownloadtailieu' ),
            'comment_notes_after' => '',
            'comment_field' => '<div class="comment-form-comment"><textarea id="comment" name="comment" placeholder="Nội dung bình luận" aria-required="true" class="form-control"></textarea></div>',
            'class_form' => 'form',
            'class_submit' => 'btn btn-default displayblock',
            'logged_in_as' => '',
            'comment_notes_after' => '',
            'submit_field' => '<span class="form-submit ">%1$s %2$s</a>',
            'format' => 'html5',
            'title_reply_to'    => __( 'Trả lời bình luận đến %s' ),
            'cancel_reply_link' => __( 'Hủy bình luận' ),
        );
        comment_form( $comments_args , $id);

        $cmt = wp_count_comments( get_the_ID() );
        echo "<div class='count-comment'>".$cmt->total_comments." bình luận</div>";

        $comments = get_comments(array(
            'post_id' => $id,
            'status' => 'approve',
        ));

        $list_args = [
            'walker'            => new Walker_Comment(),
            'max_depth'         => '3',
            'style'             => 'ul',
            'callback'          => null,
            'end-callback'      => null,
            'type'              => 'all',
            'reply_text'        => 'Trả lời',
            'page'              => '1',
            'per_page'          => '10',
            'avatar_size'       => 48,
            'reverse_top_level' => false,
            'reverse_children'  => '',
            'format'            => 'html5',
            'short_ping'        => false,  
            'echo'              => true,
            'callback' => [$this, 'mytheme_comment']
        ];

        wp_list_comments( $list_args, $comments );
	}

	public function mytheme_comment($comment, $args, $depth) {
	    if ( 'div' === $args['style'] ) {
	        $tag       = 'div';
	        $add_below = 'comment';
	    } else {
	        $tag       = 'li';
	        $add_below = 'div-comment';
	    }
	    ?>
	    <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
	    <?php if ( 'div' != $args['style'] ) : ?>
	        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	    <?php endif; ?>
	    <div class="comment-author vcard">
	        <?php 
	        	if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] );
	        	printf( __( '<cite class="fn text-red author">%s</cite> <span class="says">says:</span>' ), get_comment_author_link() ); 
	        ?>
	    </div>
	    <div class="comment-date">
	    	<?php printf( __('%1$s'), date_format(date_create($comment->comment_date), 'd/m/Y'),  '' );  ?>
	    </div>
	    <?php if ( $comment->comment_approved == '0' ) : ?>
	         <em class="comment-awaiting-moderation"><?php _e( 'Bình luận đang chờ người quản trị.' ); ?></em>
	          <br />
	    <?php endif; ?>

	    <div class="comment-content clearfix">
	    <?php 
	        comment_text(); 
	    ?>
		</div>

	    <div class="reply text-red">
	        <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
	    </div>
	    <?php if ( 'div' != $args['style'] ) : ?>
	    </div>
	    <?php endif; ?>
	    <?php
	}

    public function widget_register() {
        $sidebars = [
            [
                'name'          => __('Slider', 'slider'),
                'id'            => 'slider',
                'description'   => __('This is sidebar for header text', 'download_tailieu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer 1', 'footer'),
                'id'            => 'footer_1',
                'description'   => __('This is sidebar for header text', 'download_tailieu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer 2', 'footer'),
                'id'            => 'footer_2',
                'description'   => __('This is sidebar for header text', 'download_tailieu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer 3', 'footer'),
                'id'            => 'footer_3',
                'description'   => __('This is sidebar for header text', 'download_tailieu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer 4', 'footer'),
                'id'            => 'footer_4',
                'description'   => __('This is sidebar for header text', 'download_tailieu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer 5', 'footer'),
                'id'            => 'footer_5',
                'description'   => __('This is sidebar for header text', 'download_tailieu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer 6', 'footer'),
                'id'            => 'footer_6',
                'description'   => __('This is sidebar for header text', 'download_tailieu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Quy đổi thẻ nạp', 'footer'),
                'id'            => 'convert-card',
                'description'   => __('Hiển thị trên trang thanh toán bằng thẻ nạp', 'download_tailieu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Hiển thị khối gói tiết kiệm', 'footer'),
                'id'            => 'block-package',
                'description'   => __('Hiển thị trên trang thanh toán bằng thẻ nạp', 'download_tailieu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s col-12 col-sm-4 col-md-4 col-xl-4">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
        ];
        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }

    public function SearchFilter($query) {
        if ($query->is_search) {
            $paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;

            $query->set('post_type', 'wpdmpro');

            $query->set('posts_per_page', 20);

            $query->set('paged', $paged);
        }
        return $query;
    }
    public function wpdocs_after_setup_theme() {

        add_theme_support( 'html5', array( 'search-form' ) );
    }
    public function addUserExtendMoney($user) {
        $user_ex = UserExtend::where('user_id', $user->ID)->first();
        if($user_ex) {
        ?>
        <h3><?php _e('Thông tin khác của người dùng','wpdm-premium-package'); ?></h3>
        <table class="form-table user_extend_form">
            <tbody>
                <tr class="hidden">
                    <th>
                        <label for="inp_savemoney">
                        <?php 
                            echo __('Tài khoản tiết kiệm', 'downloadtailieu');        
                        ?>
                        </label>
                    </th>
                    <td>
                        <input type="text" id="inp_savemoney" name="inp_savemoney" value="<?php echo ($user_ex->save_money); ?>">
                        <span class="">vnđ</span>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="inp_hasmoney">
                        <?php 
                            echo __('Tài khoản nạp thẻ', 'downloadtailieu');        
                        ?>
                        </label>
                    </th>
                    <td>
                        <input type="text" id="inp_hasmoney" name="inp_hasmoney" value="<?php echo ($user_ex->has_money); ?>">
                        <span class="">vnđ</span>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="inp_dob">
                        <?php 
                            echo __('Ngày tháng năm sinh', 'downloadtailieu');        
                        ?>
                        </label>
                    </th>
                    <td>
                        <input type="date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" format="YY-mm-dd" id="inp_dob" name="inp_dob" value="<?php echo (!empty($user_ex->dob) ? $user_ex->dob : '1980-01-01'); ?>">
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="inp_job">
                        <?php 
                            echo __('Nghề nghiệp', 'downloadtailieu');        
                        ?>
                        </label>
                    </th>
                    <td>
                        <select class="inp_job fix-height-30 form-control" name="inp_job" id="inp_job" style="width: auto;">
                            <option value="hocsinh" <?php echo ($user_ex->job == 'hocsinh' ? 'selected' : ''); ?>>Học sinh</option>
                            <option value="giaovien" <?php echo ($user_ex->job == 'giaovien' ? 'selected' : ''); ?>>Giáo viên</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="inp_subject">
                        <?php 
                            echo __('Bộ môn', 'downloadtailieu');        
                        ?>
                        </label>
                    </th>
                    <td>
                        <input type="text" id="inp_subject" name="inp_subject" value="<?php echo ($user_ex->subject); ?>">
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="inp_school">
                        <?php 
                            echo __('Trường học', 'downloadtailieu');        
                        ?>
                        </label>
                    </th>
                    <td>
                        <input type="text" id="inp_school" name="inp_school" value="<?php echo ($user_ex->school); ?>">
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="inp_address">
                        <?php 
                            echo __('Địa chỉ', 'downloadtailieu');        
                        ?>
                        </label>
                    </th>
                    <td>
                        <input type="text" id="inp_address" name="inp_address" value="<?php echo ($user_ex->address); ?>">
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="inp_phone">
                        <?php 
                            echo __('Điện thoại', 'downloadtailieu');        
                        ?>
                        </label>
                    </th>
                    <td>
                        <input type="text" id="inp_phone" name="inp_phone" value="<?php echo ($user_ex->phone); ?>">
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="inp_paymentinfo">
                        <?php 
                            echo __('Thông tin tài khoản', 'downloadtailieu');        
                        ?>
                        </label>
                    </th>
                    <td>
                        <textarea id="inp_paymentinfo" class="form-control" name="inp_paymentinfo" rows="5" style="width: 100%;"><?php echo $user_ex->payment_info; ?></textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
        }
    }
    public function saveUserExtendMoney($user_id) {
        $user_ex = UserExtend::where('user_id', $user_id)->first();
        if($user_ex) {
            // $user_ex->save_money = $_POST['inp_savemoney'];
            $user_ex->has_money = $_POST['inp_hasmoney'];
            $user_ex->dob = $_POST['inp_dob'];
            $user_ex->job = $_POST['inp_job'];
            $user_ex->subject = $_POST['inp_subject'];
            $user_ex->address = $_POST['inp_address'];
            $user_ex->school = $_POST['inp_school'];
            $user_ex->phone = $_POST['inp_phone'];
            $user_ex->payment_info = $_POST['inp_paymentinfo'];
            $user_ex->save();
        }
    }
}

new Setting;