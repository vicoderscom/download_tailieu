<?php 

namespace Database\Migration;

use Database\Migration\ExeDB;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
* 
*/
class AddTransactionHistoryTable extends ExeDB
{
	public $table = 'transaction_history';

	public function __construct()
	{
		parent::__construct();
	}

	public function up()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (!Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->create($table_name, function($table){
				$table->increments('id');
				$table->integer('user_id');
				$table->integer('doc_id');
				$table->string('trans_id', 100);
				$table->boolean('status')->default(0)->comment('0: pending, 1: success');
				$table->timestamps();
			});
		}		
	}

	public function down() {
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->drop($table_name);
		}
	}
}