<?php 

namespace Database\Migration;

use Database\Migration\ExeDB;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
* 
*/
class AddUserExtendTable extends ExeDB
{
	public $table = 'user_extend';

	public function __construct()
	{
		parent::__construct();
	}

	public function up()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (!Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->create($table_name, function($table){
				$table->increments('id');
				$table->integer('user_id')->unique();
				$table->date('dob')->comment('date of birth')->nullable();
				$table->string('job', 50)->default('hocsinh')->comment('hocsinh - giaovien - khac');
				$table->string('subject')->nullable();
				$table->string('school')->nullable();
				$table->string('phone')->nullable();
				$table->text('address')->nullable();
				$table->text('payment_info')->nullable();
				$table->string('save_money', 50)->default(0);
				$table->string('has_money', 50)->default(0);
				$table->timestamps();
			});
		}		
	}

	public function down() {
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->drop($table_name);
		}
	}
}