<?php

use Database\RunDatebase;

$app = require_once __DIR__ . '/bootstrap/app.php';

if (!session_id()) {
    session_start();
}

if(isset($_GET['run'])) {
	$rundb = new RunDatebase();
	switch ($_GET['run']) {
		case 'up':
			$rundb->up();
			break;
		case 'down':
			$rundb->down();
			break;
		case 'seeder':
			$rundb->seeder();
			break;
		default:
			break;
	}
}