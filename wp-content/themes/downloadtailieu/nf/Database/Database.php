<?php

namespace App\Database;

use NF\Facades\App;

class Database
{
    public function __construct()
    {
        $manager = App::make('DBManager');
        $manager->bootEloquent();
        if (method_exists($this, 'up')) {
            register_activation_hook($manager->plugin_file, [$this, 'up']);
        }
        if (method_exists($this, 'down')) {
            register_uninstall_hook($manager->plugin_file, [$this, 'down']);
        }
    }
}

// namespace App\Database;

// use Illuminate\Database\Capsule\Manager as Capsule;
// use NFWP\Database\NFDatabase;

// class Database extends NFDatabase
// {
//     public function up()
//     {
//         global $wpdb;
//         $table_name = NF_TICKET_ORDER_TABLE_NAME;
//         if (!Capsule::schema()->hasTable($table_name)) {
//             Capsule::schema()->create($table_name, function ($table) {
//                 $table->increments('id');
//                 $table->integer('order_id');
//                 $table->string('type', 40);
//                 $table->string('amount', 10);
//                 $table->string('transaction_id', 40);
//                 $table->string('gateway', 40);
//                 $table->string('token', 32);
//                 $table->timestamps();
//             });
//         } else {
//             // Capsule::schema()->drop($table_name);
//         }
//     }
// }
