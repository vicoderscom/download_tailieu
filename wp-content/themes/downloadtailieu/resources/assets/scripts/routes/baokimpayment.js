$(document).ready(function() {
    $('#submit-baokim').on('click', function(event) {
        var homenetwork = $('input[name=inp_homenetwork]:checked').val();
        var txtpin = $('#txtpin').val();
        if (txtpin == '') {
            var str_pin = '';
            str_pin += '<div class="text-red">Mã pin không được để trống</div>';
            $('.error-txtpin').html(str_pin);
        }

        var txtseri = $('#txtseri').val();
        if (txtseri == '') {
            var str_seri = '';
            str_seri += '<div class="text-red">Số seri không được để trống</div>';
            $('.error-txtseri').html(str_seri);
        }

        $.ajax({
            url: ajax_url,
            type: "POST",
            data: {
                action: 'baokim_charge_card',
                inp_homenetwork: homenetwork,
                txtpin: txtpin,
                txtseri: txtseri
            },
            beforeSend: function() {
                $('.spinner-loading.fa-spinner').removeClass('hide');
                $('#submit-baokim').addClass('hide');
            },
            complete: function() {
                $('#submit-baokim').removeClass('hide');
                $('.spinner-loading.fa-spinner').addClass('hide');
            },
            success: function(response) {
                var res_html = '';
                var _respon = $.parseJSON(response);
                if (_respon.status == 200) {
                    res_html = '<div class="alert alert-success">' + _respon.result.message + '</div>';
                } else {
                    res_html = '<div class="alert alert-danger">' + _respon.result.message + '</div>';
                }
                $('.result-payment').html(res_html);
                $("html, body").animate({ scrollTop: 0 }, "slow");
            },
            error: function() {
                var res_html_fail = '<div class="alert alert-danger">Có lỗi xảy ra. Hãy kiểm tra lại các thông tin.</div>';
                $('.result-payment').html(res_html_fail);
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
        });
    });

    if($('#enoughmoney .btn-confirm-payment').length === 1) {
        $('#enoughmoney .btn-confirm-payment').on('click', function(event){
            var line_message = $('.show-message');
            var uid = $('#inp_uid').val();
            if(uid == '' || uid == null) {
                var error_uid = '<div class="alert alert-danger" role="alert">';
                error_uid += '<strong>Bạn chưa đăng nhập tài khoản. Hãy đăng nhập ngay để download tài liệu này.</strong>';
                error_uid += '</div>';
                line_message.html(error_uid);
            }
            var pid = $('#inp_pid').val();
            if(pid == '' || pid == null) {
                var error_pid = '<div class="alert alert-danger" role="alert">';
                error_pid += '<strong>Không xác định được tài liệu cần tải xuống.</strong>';
                error_pid += '</div>';
                line_message.html(error_pid);
            }

            var redirect_current = $('#redirect_current').val();
            if(redirect_current == '' || redirect_current == null) {
                redirect_current = base_url;
            }

            $.ajax({
                url: ajax_url,
                type: "POST",
                data: {
                    action: 'create_transaction',
                    uid: uid,
                    pid: pid,
                    redirect_current: redirect_current
                },
                beforeSend: function() {
                    $('.preload-handle-payment').removeClass('hide');
                },
                complete: function() {
                    $('.preload-handle-payment').removeClass('show').addClass('hide');
                },
                success: function(response) {
                    var res_html = '';
                    var _respon = $.parseJSON(response);
                    if (_respon.code == 200) {
                        res_html = '<div class="alert alert-success">' + _respon.message + '</div>';
                        $('.wrap-btn-enough').html(_respon.btn_dl);
                    } else {
                        res_html = '<div class="alert alert-danger">' + _respon.message + '</div>';
                    }
                    $('.show-message').html(res_html);
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                },
                error: function() {
                    var res_html_fail = '<div class="alert alert-danger"><strong>Có lỗi xảy ra. Hãy kiểm tra lại các thông tin.</strong></div>';
                    $('.show-message').html(res_html_fail);
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            });
        });
    }
});