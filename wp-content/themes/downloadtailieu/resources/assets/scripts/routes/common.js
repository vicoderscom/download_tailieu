// using this import type for require third 3rd. For example:
// import Wow from 'wow.js';

// or you can specify the script file which you want to import
import meanmenu from 'jquery.meanmenu/jquery.meanmenu.js';

export default {
    init() {
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9&appId=" + FB_id;
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            // var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            // (function(){
            // var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            // s1.async=true;
            // s1.src='https://embed.tawk.to/59d4b0ea4854b82732ff381e/default';
            // s1.charset='UTF-8';
            // s1.setAttribute('crossorigin','*');
            // s0.parentNode.insertBefore(s1,s0);
            // })();
            
            $('.btn-fb-dialog').css({'display' : 'block', 'z-index' : '1000'});
            $('.fb-iframe').css({'display' : 'none'});
            var height_ifr = $('.fb-iframe').height();
            $('.btn-fb-dialog').click(function(){
                var show = $('.fb-iframe').attr('data-show');
                if(show == 'none') {
                    $('.fb-iframe').css({'display' : 'block','position': 'fixed', 'z-index' : '1000', 'bottom' : '0', 'right' : '0'});
                    $('.btn-fb-dialog').css({'position' : 'fixed', 'bottom' : height_ifr});
                    $('.fb-iframe').attr('data-show', 'show');
                    $('.btn-fb-dialog').html('Đóng <i class="fa fa-times" aria-hidden="true"></i>');
                } else {
                    $('.fb-iframe').attr('data-show', 'none');
                    $('.fb-iframe').css({'display' : 'none', 'bottom' : '0'});
                    $('.btn-fb-dialog').css({'bottom' : '0'});
                    $('.btn-fb-dialog').text('Gửi tin nhắn cho chúng tôi ');
                }
            });

            $('.btn-fb-dialog-mobile').click(function(){
                var show = $('.fb-iframe').attr('data-show');
                if(show == 'none') {
                    $('.fb-iframe').css({'display' : 'block','position': 'fixed', 'z-index' : '1000', 'bottom' : '0', 'right' : '0'});
                    $('.btn-fb-dialog-mobile').css({'position' : 'fixed', 'bottom' : height_ifr});
                    $('.fb-iframe').attr('data-show', 'show');
                    $('.btn-fb-dialog-mobile').html('<i class="fa fa-times" aria-hidden="true"></i>');
                } else {
                    $('.fb-iframe').attr('data-show', 'none');
                    $('.fb-iframe').css({'display' : 'none', 'bottom' : '0'});
                    $('.btn-fb-dialog-mobile').css({'bottom' : '15px'});
                    $('.btn-fb-dialog-mobile').html('<i class="fa fa-envelope-open" aria-hidden="true"></i>');
                }
            });
        },
        finalize() {
            $('.vc-class-menu1  .menu-section').meanmenu({
                meanScreenWidth: "992",
                meanMenuContainer: ".mobile-menu",
            });

            var scroll = $('div').width();
            if (scroll >= 992) {
                var height = $('.vc-class-menu1  .menu-section > ul > li .sub-menu li .sub-menu');
                var height2 = height.height();
                var $content = $('.vc-class-menu1  .menu-section > ul > li .sub-menu');
                $content.height(height2);
            }

            // $('.vc-class-menu1  .menu-section > ul > li').hover(function() {
            //     $(this).find('.sub-menu > li:first-child > .sub-menu').css('visibility', 'visible');
            // }, function(e) {
            //     $(this).find('.sub-menu > li:first-child > .sub-menu').css('visibility', 'hidden');
            // });

            $('.vc-class-menu1 .menu-section ul li').hover(function() {
                $(this).find('ul:first').show(400).css({ 'visibility': 'visible' });
            }, function() {
                $(this).find('ul:first').css({ 'display': 'none' });
            });

            $(".form-tooltip").tooltip({ placement: 'right'});

            $(window).on('scroll', function() {
                if ($(window).scrollTop() > 20) {
                    $("#back-to-top").css("display", "block");
                } else {
                    $("#back-to-top").css("display", "none");
                }
            });
            if ($('#back-to-top').length) {
                $("#back-to-top").on('click', function() {
                    $('html, body').animate({
                        scrollTop: $('html, body').offset().top
                    }, 1000);
                });
            }

        },};
