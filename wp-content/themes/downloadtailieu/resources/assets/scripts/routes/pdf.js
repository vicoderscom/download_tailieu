var SINGLEPDF = {
    pdf: null,
    pageNum: 1,
    pageRendering: false,
    pageNumPending: null,
    scale: 2,
    item: 0,
    width: '100%',
    height: '800px',
    urlPreview: null,
    //==========
    init: function(arr_data) {
    	var _this = this;
    	arr_data.forEach(function(val, key_each){
	        _this.item = val.key;
	        _this.width = val.width;
	        _this.height = val.height;
	        _this.scale = val.scale;
	        _this.urlPreview = val.urlPreview;
	    	_this.createElementView();
	        _this.handlePdfJs();
            _this.eventButtonControl();
	        _this.item = 0;
	        _this.width = '100%';
	        _this.height = '800px';
	        _this.scale = 2;
	        _this.urlPreview = null;
    	});
    },
    eventButtonControl: function() {
        var _this = this;
        $('#prev-vframe-' + _this.item).on('click', function() {
            _this.onPrevPage();
        });
        $('#next-vframe-' + _this.item).on('click', function() {
            _this.onNextPage();
        });
    },
    createElementView: function() {
        var vframe = $('#vframe-' + this.item);
        if (vframe.length) {
            if (!vframe.find('#show-vframe-' + this.item).length) {
                var css = 'height:' + this.height + ';';
                css += 'width: ' + this.width + ';';
                var initCanvas = document.createElement('canvas');
                initCanvas.id = 'show-vframe-' + this.item;
                initCanvas.style.cssText = css;
                vframe.append(initCanvas);
            }
            if (!$('#paginate-wrap-' + this.item).length) {
                var css_page_wrap = 'position: absolute;left: 0;-webkit-border-radius: 3px;border-radius: 3px;bottom: 0px;z-index: 10;overflow: hidden;opacity:0.7';
                css_page_wrap += 'height: 41px;background-color: #000;color: #fff; padding: 10px; opacity: 0.5;';
                var paginate_wrap = document.createElement('div');
                paginate_wrap.id = 'paginate-wrap-' + this.item;
                paginate_wrap.style.cssText = css_page_wrap;
                vframe.append(paginate_wrap);

                var btnPaginate = '<span id="prev-vframe-' + this.item + '"><i  class="fa fa-angle-double-left" aria-hidden="true"></i> Prev</span> | <span id="next-vframe-' + this.item + '">Next <i  class="fa fa-angle-double-right" aria-hidden="true"></i></span>&nbsp; &nbsp;<span>Page: <span id="page_num_vframe_' + this.item + '"></span> / <span id="page_count_vframe_' + this.item + '"></span></span>';
                paginate_wrap.innerHTML = btnPaginate;
                document.getElementById('prev-vframe-' + this.item).style.cssText = 'cursor: pointer';
                document.getElementById('next-vframe-' + this.item).style.cssText = 'cursor: pointer';
            }
        }
    },

    handlePdfJs: function() {
        var _this = this;
        var loadingTask = PDFJS.getDocument(this.urlPreview);
        loadingTask.promise.then(function(pdf) {
            _this.pdf = pdf;
            document.getElementById('page_count_vframe_' + _this.item).textContent = pdf.numPages;
            _this.renderPage(_this.pageNum);
        }, function(reason) {
            console.error(reason);
        });
    },
    renderPage: function(pageNumber) {
        var _this = this;
        _this.pageRendering = true;
        _this.pdf.getPage(pageNumber).then(function(page) {
            var viewport = page.getViewport(_this.scale);
            var canvas = document.getElementById('show-vframe-' + _this.item);
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            var renderContext = {
                canvasContext: context,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);
            renderTask.promise.then(function() {
                _this.pageRendering = false;
                if (_this.pageNumPending !== null) {
                    _this.renderPage(_this.pageNumPending);
                    _this.pageNumPending = null;
                }
            });
        });
        document.getElementById('page_num_vframe_' + _this.item).textContent = pageNumber;
    },

    queueRenderPage: function(num) {
        if (this.pageRendering) {
            this.pageNumPending = num;
        } else {
            this.renderPage(num);
        }
    },

    /**
     * Displays previous page.
     */
    onPrevPage: function() {
        if (this.pageNum <= 1) {
            return;
        }
        this.pageNum--;
        this.queueRenderPage(this.pageNum);
    },

    /**
     * Displays next page.
     */
    onNextPage: function() {
        if (this.pageNum >= this.pdf.numPages) {
            return;
        }
        this.pageNum++;
        this.queueRenderPage(this.pageNum);
    }
};

var VPDF = {
    constructor() {
        var buildPDF = '';
        var arr_data = [];
        var event = new Event('PDFJS_LOADDED');

        if (document.getElementById('build-pdfjs') === undefined || document.getElementById('build-pdfjs') === null) {
            buildPDF = document.createElement('script');
            buildPDF.id = 'build-pdfjs';
            buildPDF.src = 'http://mozilla.github.io/pdf.js/build/pdf.js';
            buildPDF.type = 'text/javascript';
            buildPDF.onload = function() {
                PDFJS.workerSrc = 'http://mozilla.github.io/pdf.js/build/pdf.worker.js';
                document.dispatchEvent(event);
            }
            document.head.appendChild(buildPDF);
        }

        if (window.PDFJS !== undefined) {
        	$('div[vc-frame^="vpdf"]').each(function(key, value) {
                var urlPreview = $(value).attr('vc-urlPreview');
                var width = $(value).attr('vc-width');
                var height = $(value).attr('vc-height');
                var scale = $(value).attr('vc-scale');
                if (!$('#vframe-' + key).length) {
                    var initDivVframe = document.createElement('div');
                    initDivVframe.id = 'vframe-' + key;
                    initDivVframe.style.cssText = 'position: relative;';
                    $(value).append(initDivVframe);
                }
                arr_data.push({
            		key: key,
            		urlPreview: urlPreview,
            		width: width,
            		height: height,
            		scale: scale
            	});
            });
        	SINGLEPDF.init(arr_data);
        }
        else {
            document.addEventListener('PDFJS_LOADDED', function(e) {
                $('div[vc-frame^="vpdf"]').each(function(key, value) {
	                var urlPreview = $(value).attr('vc-urlPreview');
	                var width = $(value).attr('vc-width');
	                var height = $(value).attr('vc-height');
	                var scale = $(value).attr('vc-scale');
	                if (!$('#vframe-' + key).length) {
	                    var initDivVframe = document.createElement('div');
	                    initDivVframe.id = 'vframe-' + key;
	                    initDivVframe.style.cssText = 'position: relative;';
	                    $(value).append(initDivVframe);
	                }
	                arr_data.push({
	            		key: key,
	            		urlPreview: urlPreview,
	            		width: width,
	            		height: height,
	            		scale: scale
	            	});
	            });
	        	SINGLEPDF.init(arr_data);
            });
        }
    }
}

new VPDF.constructor();