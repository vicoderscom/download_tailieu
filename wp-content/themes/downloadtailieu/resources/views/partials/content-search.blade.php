<div class="search">
    <div class="container">
        @php
            global $query_string;
            $query_args = explode("&", $query_string);
            $search_query = array();

            foreach($query_args as $key => $string) {
              $query_split = explode("=", $string);
              $search_query[$query_split[0]] = urldecode($query_split[1]);
            } // foreach

            if (!empty($search_query[$query_split[0]])){
       	
            $the_query = new WP_Query($search_query);

            if ( $the_query->have_posts() ) : 
				echo "<div class='result_search'>" . __('Kết quả tìm kiếm với từ khóa: ', 'search') . get_query_var('s'). ' '. '('. $the_query->found_posts .')' . 'kết quả'."</div>";
            @endphp
            <!-- the loop -->

            <div class="row list-item">    
	            @php

	                while ( $the_query->have_posts() ) : $the_query->the_post();

	                $images = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

	                $link = get_permalink($post->ID);
	            @endphp
	                <div class="col-xs-12 col-md-3 col-sm-6 recomm-homepage">
						<div class="images-item">
							<a href="{{ $link }}">
								<img class="icon-topic" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/icon-work.png" alt="">

								<img class="avatar-item" style="background-image:url({{  $images }}); " src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/item-home.png" alt="">
							</a>
						</div>
						<div class="info-item">
							<a href="#">
								<p class="title-item">{{ get_the_title() }}</p>
							</a>
						</div>
					</div>
	            @php endwhile; @endphp
            </div>

            <div class="paginate">
	            @php
	            	
		            $total_pages = $the_query->max_num_pages;

		            if ($total_pages > 1) :

		                $current_page = max(1, $paged);

		                echo paginate_links(array(
					        'base' => @add_query_arg('trang','%#%'),
					        'format' => '?trang=%#%',
					        'current' => $current_page,
					        'total' => $total_pages,
		                    'prev_text'    => __('<'),
		                    'next_text'    => __('>')
					    ));
	            @endphp
	            @php
	            	endif;
	            @endphp
	            @php wp_reset_postdata(); @endphp
	        </div>
	        
            <!-- end of the loop -->
            @php wp_reset_postdata(); @endphp

			
        @php else : @endphp
            <div class="alert alert-warning">{{ __('Xin lỗi, không tìm thấy kết quả nào với từ khóa : '. get_query_var('s') .' ', 'vicoders') }}</div>
        @php endif; }@endphp
    </div>
</div>