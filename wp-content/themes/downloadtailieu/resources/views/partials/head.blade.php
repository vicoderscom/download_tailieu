<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/favicon.png" type="image/x-icon" />
    @php
    	wp_head();
    @endphp
    <script type="text/javascript">
    	var FB_id = '209789646227641';
    	var base_url = '{{ get_site_url() }}';
    </script>
</head>
