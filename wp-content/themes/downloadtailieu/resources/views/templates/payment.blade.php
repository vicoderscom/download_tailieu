@extends('layouts.app')

@section('content')
@php
    global $current_user, $wpdb;
@endphp
<div class="payment-section">
    <div class="container">
        <div class="row info-payment">
            <div class="col-12 col-sm-6 col-md-6 col-xl-6">
                <div class="result-payment"></div>
                <div class="text-red">
                    Nạp tiền bằng thẻ điện thoại
                </div>
                <form class="form-horizontal form-bk" role="form" method="post" action="">
                    <div class="form-group">
                        <label class="control-label opensan">* Loại thẻ ( Tích vào ô chọn loại thẻ Viettel, Vinaphone, Mobiphone)</label>
                        <div class="row pad-l-0-i">
                            <div class="viettel-block col-2 text-center">
                                <img class="icon-topic" src="<?php echo get_stylesheet_directory_uri() . '/resources/assets/images/payments/viettel.png'; ?>" alt="">
                                <p><input type="radio" name="inp_homenetwork" value="viettel" checked="checked"></p>
                            </div>
                            <div class="vina-block col-2 push-sm-2 text-center">
                                <img class="icon-topic" src="<?php echo get_stylesheet_directory_uri() . '/resources/assets/images/payments/vinaphone.png'; ?>" alt="">
                                <p><input type="radio" name="inp_homenetwork" value="vinaphone"></p>
                            </div>
                            <div class="mobi-block col-2 push-sm-2 text-center">
                                <img class="icon-topic" src="<?php echo get_stylesheet_directory_uri() . '/resources/assets/images/payments/mobiphone.png'; ?>" alt="">
                                <p><input type="radio" name="inp_homenetwork" value="mobiphone"></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-tooltip">
                        <label for="txtpin" class="control-label opensan">* Mã thẻ</label>
                        <div class="">
                            <input type="text" class="form-control" id="txtpin" name="txtpin" placeholder="Điển mã thẻ viết liền (0 cách trống, 0 gạch ngang)" data-toggle="tooltip" data-title="Mã số sau lớp bạc mỏng"/>
                            <div class="error-txtpin"></div>
                        </div>
                    </div>
                    <div class="form-group form-tooltip">
                        <label for="txtseri" class="control-label opensan">* Số seri</label>
                        <div class="">
                            <input type="text" class="form-control" id="txtseri" name="txtseri" placeholder="Điển số seri viết liền (0 cách trống, 0 gạch ngang)" data-toggle="tooltip" data-title="Mã seri nằm sau thẻ" >
                            <div class="error-txtseri"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="">
                            {{-- <input type="hidden" name="user_id" id="user_id" value={{ $current_user->ID }}> --}}
                            <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading hide text-center center"></i>
                            <button type="button" class="btn btn-primary" name="napthe" id="submit-baokim">Nạp thẻ ngay</button>
                        </div>
                    </div>
                </form>    
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-xl-6">
                <div class="col-12 link-history">
                    <div class="link-history-text opensan">Số dư: <span class="text-red">{{ price_format($user_extend->has_money)  }} vnđ</span> <a href="{{ site_url('user-dashboard/transaction') }}">(Lịch sử giao dịch)</a></div>
                </div>
                @php 
                    if(is_active_sidebar('convert-card')) {
                        dynamic_sidebar('convert-card');
                    }
                @endphp
            </div>
        </div>

        <div class="row info-payment-ask">
            {!! $post->post_content !!}
        </div>

    </div>
</div>
@endsection
