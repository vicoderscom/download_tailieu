<?php 

global $current_user, $wpdb;
$post_id = $post->ID;
$post_current = get_queried_object();

$get_cat = [];
$category = get_the_terms($post_id, 'wpdmcategory');
if (!empty($category)) {
    foreach ($category as $cat) {
        $cat_parent = $cat->parent;

        if ($cat_parent == 0) {
            $cat_parent = $cat->term_id;
        }

        $args_cat = [
            'post_type'      => 'wpdmpro',
            'posts_per_page' => 8,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'tax_query'      => [
                [
                    'taxonomy' => 'wpdmcategory',
                    'field'    => 'id',
                    'terms'    => $cat_parent,
                ],
            ],
        ];
        $get_cat[] = new WP_Query($args_cat);
    }
}

$data = [
	'relate_docs'     => $get_cat,
	'post_current' => $post_current
];

echo view('components.sidebar-wpdmpro', $data);