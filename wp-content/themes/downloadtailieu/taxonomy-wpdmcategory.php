<?php

$object_current = get_queried_object();

$current_term = get_term($object_current->term_id, 'wpdmcategory');

$child_terms = get_terms([
    'taxonomy'   => 'wpdmcategory',
    'hide_empty' => false,
    'parent'     => $object_current->term_id,
]);

// foreach($child_terms as $key => $child) {
//     echo "<pre>";
//     var_dump($child_terms);
// }
// die;

$post_cat = [
    'post_type'      => 'wpdmpro',
    'posts_per_page' => -1,
    'post_status'    => 'publish',
    'tax_query'      => [
        [
            'taxonomy'         => 'wpdmcategory',
            'terms'            => [$object_current->term_id],
            'field'            => 'term_id',
            'operator'         => 'AND',
            'include_children' => false,
        ],
    ],
];

$get_post_categoris = new WP_Query($post_cat);

foreach ($get_post_categoris->posts as $key => $val) {
    $get_count_download  = $wpdb->get_row("select count(id) as count_dl from {$wpdb->prefix}ahm_download_stats s where s.uid = '{$current_user->ID}' and s.pid = '{$val->ID}'");
    $val->check_download = $get_count_download->count_dl;
}

$data = [
    'object_current'     => $object_current,
    'current_term'       => $current_term,
    'child_terms'        => $child_terms,
    'get_post_categoris' => $get_post_categoris,
];

view('archive-wpdmpro', $data);
